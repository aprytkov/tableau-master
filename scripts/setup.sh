docker-compose up -d
echo "Starting server..."
sleep 5

echo "Initial server configuration"
docker exec backend-tableau-master python manage.py migrate --noinput
docker exec backend-tableau-master python manage.py collectstatic --no-input --clear
docker exec backend-tableau-master python manage.py createsuperuser --noinput
docker-compose restart
echo "Initial server configuration completed"